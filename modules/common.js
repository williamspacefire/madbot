/*
* BOT Oficial da M4D SQU4D
*
* @Author William Spacefire
* @Twitter @wiliamspacefire
*
*/

exports.getChannel = (client, channelName) =>  {
    return client.guild.channels.cache.find(ch => ch.name === channelName);
}

exports.shuffle = (o) => {
    for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

exports.undefined = (obj) => {
    return typeof obj == "undefined";
}