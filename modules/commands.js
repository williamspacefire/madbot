/*
* BOT Oficial da M4D SQU4D
*
* @Author William Spacefire
* @Twitter @wiliamspacefire
*
*/

const sqlite3 = require("sqlite3");
const {prefix} = require("./../config.json");
const {MessageEmbed} = require("discord.js");
const {shuffle, undefined} = require("./common.js");

const db = new sqlite3.Database("./database.db");

db.run("CREATE TABLE members ( id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL , timestamp INTEGER DEFAULT CURRENT_TIMESTAMP, UNIQUE(name) )", (error) => {});

exports.getMessage = (message) => {
    if (message.author.bot) return;
    //if (!message.content.startsWith(prefix)) return;

	const commandBody = message.content.slice(prefix.length);
	const args = commandBody.split(' ');
    const command = args.shift().toLowerCase();
    
    if (command == "calendario") {
    
        const months = [
            "janeiro",
            "jevereiro",
            "março",
            "abriu",
            "maio",
            "junho",
            "julho",
            "agosto",
            "setembro",
            "outubro",
            "novembro",
            "dezembro"
        ];

        const weeks = [
            "domingo",
            "segunda",
            "terça",
            "quarta",
            "quinta",
            "sexta",
            "sábado"
        ]

        if (undefined(args[0]) || months.indexOf(args[0].toLowerCase()) < 0) {
            message.channel.send("Não foi possível determinar o mês indicado, verifique se digitou corretamente.");
            return;
        }

        //if (undefined(args[1]) || args[1].split(",") <= 0) {
        //    message.channel.send("Não foi possível obter a lista de nomes, verifique se digitou separando por vírgula.");
        //    return;
        //}

        let names = [];
        let original = [];

        db.serialize(() => {
            db.all("SELECT name FROM members", (e, row) => {
                console.log(row)
                for (let i = 0; i < row.length; i++) {
                    names[i] = row[i].name;
                    original[i] = row[i].name;
                }

                console.log(names);

                names = shuffle(names);
                original = shuffle(original);
                names = [].concat(original, shuffle(original));

        const monthIndex = months.indexOf(args[0].toLowerCase());
        const month = months[monthIndex].toUpperCase();

        const header = `📍 *CELENDÁRIO DAS SALAS DE ${month}* 

`;

        var template = ``;

        const footer = `*Caso alguém não possa o dia e o horário designado, favor trocar com alguém que possa e avisar com antecedência*`;
     
        for (let index = 1; index < 31; index++) {

            console.log(`Original: ${original}`)

            /*if (undefined(names[0]) || undefined(names[1]) || undefined(names[2])) {
                names = shuffle(original)
                console.log(names)
                console.log(original)
            }*/

            const day = new Date(new Date().getFullYear(), monthIndex, index);

            if (day.getMonth() != monthIndex) break;

            const week = weeks[day.getDay()];

            if (week == "terça" || week == "quarta") {
                
                template += `${week.toUpperCase()} ${index}/${monthIndex+1} (21:00H) 
1 - ${names[0]} / BERMUDA
2 - ${names[1]} / PURGATÓRIO
3 - ${names[2]} / BERMUDA

`;
                names.splice(0,1);
                names.splice(0,1);
                names.splice(0,1);
            }
        }

        message.channel.send(header + template + footer);
            });
        });

        return;
    }

	if (command == "ping") {
		message.channel.send(Date.now() - message.createdTimestamp+"ms.");
		return;
    }
    
    if (command == "ajuda") {     

        help = new MessageEmbed()
        .setTitle("COMANDOS DO BOT")
        .setDescription("O comando ajuda ainda está em desenvolvimento")
        .setColor(0xf0000);

        message.channel.send(help);

		return;
    }
    
    if (command == "guilda") {
        
        if (!undefined(args[0]) && args[0] == "adicionar") {
            if (undefined(args[1])) {
                message.channel.send("Para adicionar membros é preciso passar os nomes");
                return;
            }

            const member = args[1].split(",");
            let done = "";

            db.serialize(() => {
                for (let i = 0; i < member.length; i++) {

                    member[i] = member[i].toUpperCase();
                    let r;

                    db.get(`SELECT name FROM members WHERE name = ?`, [member[i]], (e, row) => {
                        r = row;
                    });

                    db.run(`INSERT OR IGNORE INTO members(name) VALUES(?)`, [member[i]], (err) => {
                        console.log(err);

                        if (undefined(r)) {
                            done += `${member[i]} adicionado
`;
                        } else {
                            done += `${member[i]} já está cadastrado
`;
                        }

                        if (i == member.length-1) {
                            message.channel.send(done);
                        }
                    });
                }
            });

            return;
        }

        if (!undefined(args[0]) && args[0] == "membros") {

            db.serialize(() => {

                db.all("SELECT * FROM members", (r, row) => {
                    
                    let members = "";

                    for (i = 0; i < row.length; i++) {
                        members += `${i+1} - ${row[i].name}
`;
                    }

                    message.channel.send(members);
                });
            });

            return;
        }

        if (!undefined(args[0]) && args[0] == "remover") {
            if (undefined(args[1])) {
                message.channel.send("Você precisa passar nomes a serem deletados");
                return;
            }

            const member = args[1].split(",");
        
            let members = "";

            db.serialize(() => {
                for (let i = 0; i < member.length; i++) {
                    member[i] = member[i].toUpperCase()
                    let r;

                    db.get(`SELECT name FROM members WHERE name = ?`, [member[i]], (e, row) => {
                        r = row;
                    });

                    db.run(`DELETE FROM members WHERE name = ?`, [member[i]], (e) => {
                        console.log(r);

                        if (!undefined(r)) {
                            members += `${member[i]} removido
`;
                        } else {
                            members += `${member[i]} não está cadastrado
`;
                        }

                        if (i == member.length-1) {
                            message.channel.send(members);
                        }
                    });
                }
            });

            return;
        }

        return;
    }
};