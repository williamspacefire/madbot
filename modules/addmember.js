/*
* BOT Oficial da M4D SQU4D
*
* @Author William Spacefire
* @Twitter @wiliamspacefire
*
*/

const {getChannel} = require("./common.js");
const {MessageEmbed} = require("discord.js");

exports.addMember = (member) => {
    const welcomeChannel = getChannel(member, "︱🎉︱ʙᴇᴍ-ᴠɪɴᴅᴏ");
  
    //Do nothing if the choose channel don't exists
    if (!welcomeChannel || welcomeChannel == "undefined") return;
  
    const welcome = `**SEJA BEM-VINDO AO SERVIDOR ${member.guild.name}**
Olá ${member}, seja muito bem-vindo ao nosso servidor, esperamos que se divirta muito com a gente.
        
**Bot Oficial**
Olá, eu sou o Bot Oficial da ${member.guild.name}, no que eu puder lhe ajudar é só falar comigo, envie **/ajuda** em qualquer canal de texto ou aqui mesmo no meu privado para que eu possa lhe ajudar. :heart:

**Ajuda Rápida**
Visite o canal ${getChannel(member, "︱📝︱ʀᴇɢʀᴀꜱ-ꜱᴇʀᴠɪᴅᴏʀ")} para saber as regras do nosso servidor.
Visite o canal ${getChannel(member, "︱📝︱ʀᴇɢʀᴀꜱ-ɢᴜɪʟᴅᴀ")} para saber as regras da nossa guilda.
Visite o canal ${getChannel(member, "︱💬︱ʀᴇqᴜɪꜱɪᴛᴏꜱ")} para saber os requisitos para entrar em nossa guilda.

Atenciosamente equipe ${member.guild.name} :heart:.`;

    const embed = new MessageEmbed()
    .setTitle(`${member.user.username} ENTROU NO NOSSO SERVIDOR`.toUpperCase())
    .setColor(0xff0000)
    .setAuthor(member.guild.name.toUpperCase(), member.guild.iconURL())
    .setDescription(`Seja muito bem-vindo ${member}, eu sou o ${member.guild.client.user}, eu e todos do servidor ${member.guild.name} estamos muito contentes de ter você aqui com a gente.`)
    .setImage(member.user.displayAvatarURL());

    welcomeChannel.send(embed);

    member.user.send(welcome);

    return;
}