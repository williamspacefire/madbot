/*
* BOT Oficial da M4D SQU4D
*
* @Author William Spacefire
* @Twitter @wiliamspacefire
*
*/

const {Client} = require("discord.js");
const {BOT_TOKEN} = require("./config.json");
const client = new Client();
const {getMessage} = require("./modules/commands.js");
const {addMember} = require("./modules/addmember.js");

/**
 * Everytime we receive a message on a channel
 * or in private, we process the message below.
 */
client.on("message", message => {
	getMessage(message);
});

/**
 * When a new member joins the server
 * We send a message on a Welcome channel
 * and in private with some server roles.
 */
client.on('guildMemberAdd', member => {
    addMember(member);
});

client.login(BOT_TOKEN);
